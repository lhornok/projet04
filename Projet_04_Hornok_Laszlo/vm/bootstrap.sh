#!/usr/bin/env bash

# Source : https://docs.docker.com/install/linux/docker-ce/ubuntu/
# add docker GPG key :
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

# Add repository
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# update
apt-get update

# install docker
apt-get install -y docker-ce docker-ce-cli containerd.io


# Install Gitlab-Runner
# Source : https://docs.gitlab.com/runner/install/linux-repository.html
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner
sudo cp /sharedfolders/gitlab-runner/config.toml /etc/gitlab-runner/
