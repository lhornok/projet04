#!/bin/bash

sudo docker run --detach \
  --publish 9443:443 --publish 9080:9080 --publish 922:22 --publish 8093:8093 \
  --hostname mygitlab.org \
  --name mygitlab \
  lhornok/dockergitlab
